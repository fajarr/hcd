@extends('vendor.laravolt.layouts.app')
@section('content')
    <x-titlebar title="Leaves List">
        <div class="item">
            <x-link url="{{ route('leave.create') }}">
                <i class="icon plus"></i> @lang('laravolt::action.add')
            </x-link>
        </div>
    </x-titlebar>

    {!! $table !!}
@stop
