@extends('vendor.laravolt.layouts.app')
@section('content')

    <x-titlebar title="Submission from {{ $leave->user->name . ' ' . $leave->user->middle_name . ' ' . $leave->user->last_name }}">
        <x-backlink url="{{ route('status.index') }}"></x-backlink>
    </x-titlebar>

    <x-panel>
        {!! form()->open()->horizontal() !!}

        {!! form()->dropdown('category', [
        'annual' => 'Annual',
        'sick' => 'Sick',
        'maternity' => 'Maternity',
        ], $leave->category)->label('Leave Category')->disable() !!}

        {!! form()->text('start', date('d F Y', strtotime($leave->start)))->label('Starting Date')->disable() !!}
        {!! form()->textarea('information', $leave->information)->label('Leave Information')->disable() !!}

        {!! form()->close() !!}
    </x-panel>

@stop
