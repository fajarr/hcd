@extends('vendor.laravolt.layouts.app')
@section('content')

    <x-titlebar
        title="Submission from {{ $leave->user->name . ' ' . $leave->user->middle_name . ' ' . $leave->user->last_name }}">
        <x-backlink url="{{ route('status.index') }}"></x-backlink>
    </x-titlebar>

    <x-panel>
        {!! form()->open()->horizontal() !!}

        {!! form()->dropdown('category', [
        'annual' => 'Annual',
        'sick' => 'Sick',
        'maternity' => 'Maternity',
        ], $leave->category)->label('Leave Category')->disable() !!}

        {!! form()->text('start', date('d F Y', strtotime($leave->start)))->label('Starting Date')->readonly() !!}
        {!! form()->textarea('information', $leave->information)->label('Leave Information')->disable() !!}
        {!! form()->close() !!}

        <div class="ui divider section"></div>

        <div class="ui red segment p-2">
            <h5>Submission status: {{ strtoupper($leave->status) }}</h5>
            <table>
                <tr>
                    <td>
                        <form action="{{ route('status.approve', $leave->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <button class="ui labeled icon button black">
                                <i class="check icon"></i>
                                Accept
                            </button>
                        </form>
                    </td>

                    <td>
                        <form action="{{ route('status.decline', $leave->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <button class="ui labeled icon button red">
                                <i class="ban icon"></i>
                                Reject
                            </button>
                        </form>
                    </td>
                </tr>
            </table>
        </div>

    </x-panel>
@stop
