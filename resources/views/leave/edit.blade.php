@extends('vendor.laravolt.layouts.app')
@section('content')

    <x-titlebar title="Leave Submission Form">
        <x-backlink url="{{ route('leave.index') }}"></x-backlink>
    </x-titlebar>

    <x-panel>
        {!! form()->bind($leave)->open()->put()->action(route('leave.update', $leave->id)) !!}

        {!! form()->dropdown('category', [
        'annual' => 'Annual',
        'sick' => 'Sick',
        'maternity' => 'Maternity',
        ])->label('Leave Category') !!}

        {!! form()->date('start')->label('Starting Date') !!}
        {!! form()->textarea('information')->label('Leave Information') !!}

        {!! form()->action(form()->submit(__('laravolt::action.save'))) !!}
        {!! form()->close() !!}
    </x-panel>

@stop
