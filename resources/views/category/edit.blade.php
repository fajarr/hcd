@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('category.index') }}"></x-backlink>

    <x-panel title="Edit Category">
        {!! form()->bind($category)->put(route('category.update', $category->getKey()))->horizontal()->multipart() !!}
        @include('category._form')
        {!! form()->close() !!}
    </x-panel>

@stop
