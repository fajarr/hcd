@extends('laravolt::layouts.app')

@section('content')


    <x-titlebar title="Category">
        <x-item>
            <x-link label="Tambah" icon="plus" url="{{ route('category.create') }}"></x-link>
        </x-item>
    </x-titlebar>

    {!! $table !!}
@stop
