@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('category.index') }}"></x-backlink>

    <x-panel title="Tambah Category">
        {!! form()->post(route('category.store'))->horizontal()->multipart() !!}
        @include('category._form')
        {!! form()->close() !!}
    </x-panel>

@stop
