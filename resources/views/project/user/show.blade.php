@extends('profile.component.layout')
@section('content')
    <x-titlebar title="List of Your Projects"></x-titlebar>
    {!! $table !!}
@stop
