@extends('vendor.laravolt.layouts.app')
@section('content')
    <x-titlebar title="Edit {{ $project->name }}">
        <x-backlink url="{{ route('project.index') }}"></x-backlink>
    </x-titlebar>

    <x-panel>
        {!! form()->bind($project)->open()->put()->action(route('project.update', $project->id))->horizontal()->attribute('autocomplete', 'off') !!}
        {!! form()->text('name')->placeholder('Project Name')->label('Project Name') !!}
        {!! form()->textarea('description')->placeholder('Project Description')->label('Description') !!}
        {!! form()->datepicker('start')->label('Project Starting Date')->placeholder('Starting Date') !!}
        {!! form()->datepicker('end')->label('Project End Date')->placeholder('End Date') !!}

        {!! form()->dropdown('status', [
        'prospect' => 'Prospect',
        'in progress' => 'In Progress',
        'finished' => 'Finished',
        ])->label('Project Status')->placeholder('Choose Project Status') !!}
        {!! form()->text('value')->label('Project Value') !!}

        {!! form()->checkboxGroup('user', $user)->label('Team Member') !!}

        {!! form()->action(
            form()->submit(__('laravolt::action.save')),
            form()->link(__('laravolt::action.back'), route('project.index'))) !!}
        {!! form()->close() !!}
    </x-panel>
@stop
