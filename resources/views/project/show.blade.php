@extends('vendor.laravolt.layouts.app')
@section('content')
    <x-titlebar title="{{ $project->name }}">
        <x-backlink url="{{ route('project.index') }}"></x-backlink>
    </x-titlebar>

    <x-panel>
        {!! form()->open()->horizontal() !!}
        {!! form()->text('name', $project->name)->label('Project Name')->disable() !!}
        {!! form()->textarea('description', $project->description)->label('Description')->disable() !!}
        {!! form()->text('start', date('d F Y', strtotime($project->start)))->label('Starting Date')->disable() !!}
        {!! form()->text('start', date('d F Y', strtotime($project->end)))->label('Project End Date')->disable() !!}
        {!! form()->text('status', strtoupper($project->status))->label('Project Status')->disable() !!}
        {!! form()->text('value', "Rp. " . number_format($project->value))->label('Project Value')->disable() !!}
        {!! form()->text('user', $user)->label('Team Member')->disable() !!}

        {!! form()->close() !!}
    </x-panel>
@stop
