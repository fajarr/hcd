@extends('vendor.laravolt.layouts.app')
@section('content')
    <x-titlebar title="Add New Project">
        <x-backlink url="{{ route('project.index') }}"></x-backlink>
    </x-titlebar>

    <x-panel>
        {!! form()->post(route('project.store'))->attribute('autocomplete', 'off')->horizontal() !!}
        {!! form()->text('name')->placeholder('Project Name')->label('Project Name') !!}
        {!! form()->textarea('description')->placeholder('Project Description')->label('Description') !!}
        {!! form()->datepicker('start')->label('Project Starting Date')->placeholder('Starting Date') !!}
        {!! form()->datepicker('end')->label('Project End Date')->placeholder('End Date') !!}
        {!! form()->text('text')->label('Text') !!}

        {!! form()->dropdown('status', [
        'prospect' => 'Prospect',
        'in progress' => 'In Progress',
        'finished' => 'Finished',
        ])->label('Project Status')->placeholder('Choose Project Status') !!}
        {!! form()->text('value')->label('Project Value') !!}

        {!! form()->checkboxGroup('user', $user)->label('Team Member') !!}

        {!! form()->action(
            form()->submit(__('laravolt::action.save')),
            form()->link(__('laravolt::action.back'), route('project.index'))) !!}
        {!! form()->close() !!}
    </x-panel>
@stop
