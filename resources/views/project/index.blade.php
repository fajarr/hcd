@extends('vendor.laravolt.layouts.app')
@section('content')
    <x-titlebar title="Project">
        <div class="item">
            <x-link url="{{ route('project.create') }}">
                <i class="icon plus"></i> @lang('laravolt::action.add')
            </x-link>
        </div>
    </x-titlebar>

    {!! $table !!}
@stop
