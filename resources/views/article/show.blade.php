@extends('laravolt::layouts.app')

@section('content')
    <x-titlebar title="">
        <x-backlink url="{{ route('article.index') }}"></x-backlink>
    </x-titlebar>

    <x-panel title="{{ $article->title }}">
        <div class="ui grid">
            <div class="four wide column">
                <img src="{{ asset('storage/'.$article->image) }}" alt="image" class="ui centered medium image">
            </div>

            <div class="twelve wide column">
                <p>
                    {{ $article->content }}
                </p>
            </div>
        </div>

        <div class="ui divider section"></div>
        <h5>{{ $article->category->name }}</h5>
        <small>
            Created at {{ date('d F Y', strtotime($article->created_at)) }}
            , by {{ $article->user->name . " " . $article->user->middle_name . " " . $article->user->last_name }}
        </small>
    </x-panel>
@stop
