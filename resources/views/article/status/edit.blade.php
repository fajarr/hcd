@extends('laravolt::layouts.app')

@section('content')
    <x-titlebar title="Edit {{ $article->title }}">
        <x-backlink url="{{ route('article-status.index') }}"></x-backlink>
    </x-titlebar>

    <x-panel title="">
        {!! form()->bind($article)->put(route('article-status.update', $article->getKey()))->horizontal()->multipart() !!}
        {!! form()->text('title', $article->title)->label('Title')!!}
        {!! form()->textarea('content', $article->content)->label('Content') !!}
        {!! form()->select('category_id', $category)->label('Categories')->placeholder('Choose category') !!}
        <img src="{{ asset('storage/'.$article->image) }}" alt="image" class="ui centered medium image">

        <div class="ui divider hidden"></div>

        {!! form()->file('image')->label('Insert Image') !!}

        <div class="ui divider section"></div>

        {!! form()->action([
            form()->submit('Simpan'),
            form()->link('Batal', route('category.index'))
        ]) !!}
        {!! form()->close() !!}
    </x-panel>

@stop
