@extends('laravolt::layouts.app')

@section('content')
    <x-titlebar title="">
        <x-backlink url="{{ route('article-status.index') }}"></x-backlink>
    </x-titlebar>

    <x-panel title="{{ $article->title }}">
        <div class="ui grid">
            <div class="four wide column">
                <img src="{{ asset('storage/'.$article->image) }}" alt="image" class="ui centered medium image">
            </div>

            <div class="twelve wide column">
                <p>
                    {{ $article->content }}
                </p>
            </div>
        </div>

        <div class="ui divider section"></div>
        <h5>{{ $article->category->name }}</h5>
        <small>
            Created at {{ date('d F Y', strtotime($article->created_at)) }}
            , by {{ $article->user->name . " " . $article->user->middle_name . " " . $article->user->last_name }}
        </small>

        <div class="ui red segment p-2">
        <h5>Submission status: {{ strtoupper($article->status) }}</h5>
        <table>
            <tr>
                <td>
                    <form action="{{ route('article-status.accept', $article->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <button class="ui labeled icon button black">
                            <i class="check icon"></i>
                            Approve
                        </button>
                    </form>
                </td>

                <td>
                    <form action="{{ route('article-status.cancel', $article->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <button class="ui labeled icon button red">
                            <i class="ban icon"></i>
                            Cancel
                        </button>
                    </form>
                </td>
            </tr>
        </table>
        </div>
    </x-panel>
@stop
