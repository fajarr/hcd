@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('article.index') }}"></x-backlink>

    <x-panel title="Tambah Article">
        {!! form()->post(route('article.store'))->horizontal()->multipart() !!}
        @include('article._form')
        {!! form()->close() !!}
    </x-panel>

@stop
