	{!! form()->text('title')->label('Title') !!}
	{!! form()->textarea('content')->label('Content') !!}
	{!! form()->file('image')->label('Insert Image') !!}
    {!! form()->select('category_id', $category)->label('Categories')->placeholder('Choose category') !!}

{!! form()->action([
    form()->submit('Simpan'),
    form()->link('Batal', route('article.index'))
]) !!}
