@extends('laravolt::layouts.app')

@section('content')


    <x-titlebar title="Article">
        <x-item>
            <x-link label="Tambah" icon="plus" url="{{ route('article.create') }}"></x-link>
        </x-item>
    </x-titlebar>

    {!! $table !!}
@stop
