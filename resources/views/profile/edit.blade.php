@extends('profile.component.layout')
@section('content')
    <x-titlebar title="Edit Your Profile">
        <x-backlink url="{{ route('profile.index') }}"></x-backlink>
    </x-titlebar>

    <x-panel>
        <div class="ui tabular menu top attached">
            <a class="item active" href="{{ route('profile.edit') }}">@lang('laravolt::menu.account')</a>
            <a class="item" href="{{ route('profile.password.edit')  }}">@lang('laravolt::menu.password')</a>
        </div>

        <div class="ui divider hidden"></div>

        {!! form()->bind($user)->put(route('profile.update'))->horizontal()->multipart() !!}
        <img src="{{ asset('storage/'.$user->profile->image) }}" alt="image" class="ui centered medium image">
        <div class="ui divider hidden"></div>
        @include('profile.component._form')
        {!! form()->action(
            form()->submit(__('laravolt::action.save')),
            form()->link(__('laravolt::action.back'), route('profile.index'))) !!}
        {!! form()->close() !!}
    </x-panel>
@stop
