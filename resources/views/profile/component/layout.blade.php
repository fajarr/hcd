@extends('laravolt::layouts.base')

@section('body')
    <div class="layout--app">

        @include('profile.component.topbar')
        @include('laravolt::menu.sidebar')

        <main class="content">

            <div class="content__inner">
                <div class="ui container-fluid content__body p-2">
                    @yield('content')
                </div>

            </div>
        </main>
    </div>
@endsection
