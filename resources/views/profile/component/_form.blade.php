{!! form()->file('image')->label('Change Profile Picture') !!}
{!! form()->text('telegram', $user->profile->telegram)->label('Akun Telegram') !!}
{!! form()->text('bpjs_kes', $user->profile->bpjs_kes)->label('BPJS Kesehatan') !!}
{!! form()->text('bpjs_ket', $user->profile->bpjs_ket)->label('BPJS Ketenagakerjaan') !!}
{!! form()->dropdown('gol_darah', ['a' => 'A', 'b' => 'B', 'ab' => 'AB', 'o' => 'O'], $user->profile->gol_darah)->label('Golongan Darah') !!}
{!! form()->text('ijazah', $user->profile->ijazah)->label('Nomor Ijazah') !!}
{!! form()->text('nokk', $user->profile->nokk)->label('Nomor Kartu Keluarga') !!}
{!! form()->text('npwp', $user->profile->npwp)->label('NPWP') !!}
{!! form()->text('payroll', $user->profile->payroll)->label('Rekening Payroll') !!}
{!! form()->text('place_of_birth', $user->profile->place_of_birth)->label('Tempat Lahir') !!}
