@extends('profile.component.layout')
@section('content')
    <x-titlebar title="Edit Your Profile">
        <x-backlink url="{{ route('profile.index') }}"></x-backlink>
    </x-titlebar>

    <x-panel>
        <div class="ui tabular menu top attached">
            <a class="item" href="{{ route('profile.edit') }}">@lang('laravolt::menu.account')</a>
            <a class="item active" href="{{ route('profile.password.edit')  }}">@lang('laravolt::menu.password')</a>
        </div>

        <div class="ui divider hidden"></div>

        {!! form()->bind($user)->put(route('profile.password.update')) !!}

        {!! form()->password('password_current')->label(__('laravolt::users.password_current')) !!}
        {!! form()->password('password')->label(__('laravolt::users.password_new')) !!}
        {!! form()->password('password_confirmation')->label(__('laravolt::users.password_new_confirmation')) !!}

        {!! form()->action(
            form()->submit(__('laravolt::action.save')),
            form()->link(__('laravolt::action.back'), route('profile.index'))) !!}
        {!! form()->close() !!}
    </x-panel>
@stop
