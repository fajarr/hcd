@extends('profile.component.layout')
@section('content')
    <x-titlebar title="Your Profile"></x-titlebar>

    <div class="ui grid">
        <div class="four wide column">
            <x-panel title="{{ $user->name . ' ' .  $user->middle_name . ' ' .  $user->last_name}}">
                <img src="{{ asset('storage/'.$user->profile->image) }}" alt="image" class="ui centered medium image">
            </x-panel>
        </div>

        <div class="twelve wide column">
            <x-panel title="Personal Information">
                {!! form()->open()->horizontal() !!}
                <div class="ui grid">
                    <div class="four wide column">
                        <strong>Full Name</strong>
                    </div>
                    <div class="four wide column">
                        {!! form()->text('name', $user->name)->placeholder('First Name')->readonly() !!}
                    </div>
                    <div class="four wide column">
                        {!! form()->text('middle_name', $user->middle_name)->placeholder('Middle Name')->readonly() !!}
                    </div>
                    <div class="four wide column">
                        {!! form()->text('last_name', $user->last_name)->placeholder('Last Name')->readonly() !!}
                    </div>
                </div>

                <div class="ui divider section"></div>

                <div class="ui grid">
                    <div class="row">
                        <div class="four wide column">
                            <strong>Employee ID</strong>
                        </div>
                        <div class="four wide column">
                            {!! form()->text('employee_id', $user->employee_id)->readonly() !!}
                        </div>
                        <div class="four wide column">
                            <strong>Other ID</strong>
                        </div>
                        <div class="four wide column">
                            {!! form()->text('other_id', $user->other_id)->readonly() !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="four wide column">
                            <strong>Driver's License Number</strong>
                        </div>
                        <div class="four wide column">
                            {!! form()->text('license_number', $user->license_number)->readonly() !!}
                        </div>
                        <div class="four wide column">
                            <strong>License Expiry Date</strong>
                        </div>
                        <div class="four wide column">
                            {!! form()->text('license_exp', date('d F Y', strtotime($user->license_exp)))->readonly() !!}
                        </div>
                    </div>
                </div>

                <div class="ui divider section"></div>

                <div class="ui grid">
                    <div class="row">
                        <div class="four wide column">
                            <strong>Gender</strong>
                        </div>
                        <div class="four wide column">
                            {!! form()->radioGroup('gender', ['male' => 'Male', 'female' => 'Female'], $user->gender)->addClass('disabled') !!}
                        </div>
                        <div class="four wide column">
                            <strong>Marital Status</strong>
                        </div>
                        <div class="four wide column">
                            {!! form()->dropdown('marital_status', [
                            'single' => 'Single',
                            'married' => 'Married'
                            ], $user->marital_status)->disable() !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="four wide column">
                            <strong>Nationality</strong>
                        </div>
                        <div class="four wide column">
                            {!! form()->dropdown('nationality', [
                            'indonesian' => 'Indonesian',
                            'foreign' => 'Foreign'
                            ], $user->nationality)->disable() !!}
                        </div>
                        <div class="four wide column">
                            <strong>Date of Birth</strong>
                        </div>
                        <div class="four wide column">
                            {!! form()->text('dob', date('d F Y', strtotime($user->dob)))->readonly() !!}
                        </div>
                    </div>
                </div>

                {!! form()->close() !!}
            </x-panel>
        </div>

        {{--    Additional Informations Field--}}

        <div class="row">
            <div class="right floated twelve wide column">
                <x-panel title="Additional Information">
                    {!! form()->open()->horizontal() !!}

                    {!! form()->text('telegram', $user->profile->telegram)->label('Akun Telegram')->readonly() !!}
                    {!! form()->text('bpjs_kes', $user->profile->bpjs_kes)->label('BPJS Kesehatan')->readonly() !!}
                    {!! form()->text('bpjs_ket', $user->profile->bpjs_ket)->label('BPJS Ketenagakerjaan')->readonly() !!}
                    {!! form()->dropdown('gol_darah', ['a' => 'A', 'b' => 'B', 'ab' => 'AB', 'o' => 'O'], $user->profile->gol_darah)->label('Golongan Darah')->readonly() !!}
                    {!! form()->text('ijazah', $user->profile->ijazah)->label('Nomor Ijazah')->readonly() !!}
                    {!! form()->text('nokk', $user->profile->nokk)->label('Nomor Kartu Keluarga')->readonly() !!}
                    {!! form()->text('npwp', $user->profile->npwp)->label('NPWP')->readonly() !!}
                    {!! form()->text('payroll', $user->profile->payroll)->label('Rekening Payroll')->readonly() !!}
                    {!! form()->text('place_of_birth', $user->profile->place_of_birth. ', ' . date('d F Y', strtotime($user->dob)))->label('Tempat Lahir')->readonly() !!}

                    {!! form()->close() !!}
                </x-panel>
            </div>
        </div>
    </div>
@stop
