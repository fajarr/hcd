<div class="ui grid">
    <div class="four wide column">
        <strong>Full Name</strong>
    </div>
    <div class="four wide column">
        {!! form()->text('name')->placeholder('First Name')->required() !!}
    </div>
    <div class="four wide column">
        {!! form()->text('middle_name')->placeholder('Middle Name') !!}
    </div>
    <div class="four wide column">
        {!! form()->text('last_name')->placeholder('Last Name') !!}
    </div>
</div>

<div class="ui divider section"></div>

<div class="ui grid">
    <div class="row">
        <div class="four wide column">
            <strong>Employee ID</strong>
        </div>
        <div class="four wide column">
            {!! form()->text('employee_id') !!}
        </div>
        <div class="four wide column">
            <strong>Other ID</strong>
        </div>
        <div class="four wide column">
            {!! form()->text('other_id') !!}
        </div>
    </div>

    <div class="row">
        <div class="four wide column">
            <strong>Driver's License Number</strong>
        </div>
        <div class="four wide column">
            {!! form()->text('license_number') !!}
        </div>
        <div class="four wide column">
            <strong>License Expiry Date</strong>
        </div>
        <div class="four wide column">
            {!! form()->datepicker('license_exp')->format('Y-m-d') !!}
        </div>
    </div>
</div>

<div class="ui divider section"></div>

<div class="ui grid">
    <div class="row">
        <div class="four wide column">
            <strong>Gender</strong>
        </div>
        <div class="four wide column">
            {!! form()->radioGroup('gender', ['male' => 'Male', 'female' => 'Female']) !!}
        </div>
        <div class="four wide column">
            <strong>Marital Status</strong>
        </div>
        <div class="four wide column">
            {!! form()->dropdown('marital_status', [
            'single' => 'Single',
            'married' => 'Married'
            ]) !!}
        </div>
    </div>

    <div class="row">
        <div class="four wide column">
            <strong>Nationality</strong>
        </div>
        <div class="four wide column">
            {!! form()->dropdown('nationality', [
            'indonesian' => 'Indonesian',
            'foreign' => 'Foreign'
            ]) !!}
        </div>
        <div class="four wide column">
            <strong>Date of Birth</strong>
        </div>
        <div class="four wide column">
            {!! form()->datepicker('dob')->format('Y-m-d') !!}
        </div>
    </div>
</div>

<div class="ui divider section"></div>

<div class="ui grid equal width">
    <div class="column">
        {!! form()->dropdown('job_status', [
        '1' => 'Employed',
        '2' => 'Contract',
        '3' => 'Freelance',
        '4' => 'Intern'
        ])->label('Employment Status') !!}
    </div>

    <div class="column">
        {!! form()->dropdown('supervisor_id', [
        '1' => 'Wisnu Manupraba',
        '2' => 'Atep Taopik',
        '3' => 'Dicky Puja',
        '4' => 'Bayu Hendra W',
        '5' => 'Anandia M Yudhistira'
        ])->label('Supervisor') !!}
    </div>

    <div class="column">
        {!! form()->dropdown('sub_unit', [
        '1' => 'Yogyakarta',
        '2' => 'Jakarta',
        '3' => 'Bandung'
        ])->label('Sub Unit') !!}
    </div>
</div>

<div class="ui divider section"></div>

<div class="ui grid equal width">
    <div class="column">
        @if($multipleRole)
            {!! form()->checkboxGroup('roles', $roles)->label(trans('laravolt::users.roles')) !!}
        @else
            {!! form()->radioGroup('roles', $roles)->label(trans('laravolt::users.roles')) !!}
        @endif
    </div>

    <div class="column">
        {!! form()->select('status', $statuses)->label(__('laravolt::users.status')) !!}
    </div>
</div>

<div class="ui divider section"></div>
