@extends(config('laravolt.epicentrum.view.layout'))

@section('content')
    <x-titlebar :title="__('laravolt::label.users')">
        <x-backlink url="{{ route('epicentrum::users.index') }}"></x-backlink>
    </x-titlebar>

    <div class="ui grid">
        <div class="twelve wide column centered">
            <x-panel title="Personal Detail">
                {!! form()->open()->post()->action(route('user.store')) !!}
                @include('vendor.laravolt.users._form')
                {!! form()->action(
                    form()->submit(__('laravolt::action.save')),
                    form()->link(__('laravolt::action.back'), route('epicentrum::users.index'))) !!}
                {!! form()->close() !!}
            </x-panel>
        </div>
    </div>
@endsection

@push('body')
    <script>
        $(function () {
            $('.randomize').on('click', function (e) {
                $(e.currentTarget).prev().val(Math.random().toString(36).substr(2, 8));
            });
        });
    </script>
@endpush
