<?php
use Illuminate\Support\Facades\Route;

Route::get('/', 'Home')->name('home');
Route::get('/dashboard', 'Dashboard')->name('dashboard')->middleware('auth');

Route::get('user/projects', 'UserProjectController@project')->name('user.project');
Route::get('/profile', 'ProfileController@index')->name('profile.index');
Route::get('/profile/edit', 'ProfileController@edit')->name('profile.edit');
Route::put('/profile/update', 'ProfileController@update')->name('profile.update');
Route::get('/profile/edit/password', 'ProfileController@editPassword')->name('profile.password.edit');
Route::put('/profile/update/password', 'ProfileController@updatePassword')->name('profile.password.update');


Route::resource('leave', 'LeaveController');
Route::resource('status', 'StatusController')->except(['create', 'store']);
Route::resource('user', 'UserController');
Route::resource('project', 'ProjectController');
Route::resource('article', 'ArticleController');
Route::resource('category', 'CategoryController');
Route::resource('article-status', 'ArticleStatusController')->except(['create', 'store', 'edit', 'update']);

Route::put('article-status/{status}/accept', 'ArticleStatusController@accept')->name('article-status.accept');
Route::put('article-status/{status}/cancel', 'ArticleStatusController@cancel')->name('article-status.cancel');
Route::put('status/{status}/approve', 'StatusController@approve')->name('status.approve');
Route::put('status/{status}/decline', 'StatusController@decline')->name('status.decline');
