<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Profile;
use Illuminate\Support\Facades\Hash;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $name = $faker->firstName;
    return [
        'name' => $name,
        'middle_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => strtolower($name) . '@javan.com',
        'password' => Hash::make('password'), // password
        'employee_id' => $faker->numerify('#####'),
        'other_id' => $faker->numerify('#######'),
        'license_number' => $faker->numerify('##########'),
        'license_exp' => $faker->date('Y-m-d'),
        'gender' => $faker->randomElement(['male', 'female']),
        'marital_status' => $faker->randomElement(['single', 'married']),
        'nationality' => $faker->randomElement(['indonesian', 'foreign']),
        'dob' => $faker->dateTimeBetween('1990-01-01', '1999-12-31')->format('Y-m-d'),
        'job_status' => $faker->randomElement(['1', '2', '3', '4']),
        'supervisor_id' => $faker->randomElement(['1', '2', '3', '4', '5']),
        'sub_unit' => $faker->randomElement(['1', '2', '3']),
        'status' => 'ACTIVE',
        'email_verified_at' => now(),
        'created_at' => now(),
        'updated_at' => now(),
        'remember_token' => Str::random(10),
    ];
});

$factory->define(Profile::class, function () {
    $user = factory(User::class)->create()->id;
    DB::table('acl_role_user')->insert([
        'role_id' => '2',
        'user_id' => $user,
    ]);
    return [
        'user_id' => $user
    ];

});
