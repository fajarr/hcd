<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SupervisorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('supervisors')->insert([
            'name' => 'Wisnu Manupraba',
        ]);

        DB::table('supervisors')->insert([
            'name' => 'Atep Taopik',
        ]);

        DB::table('supervisors')->insert([
            'name' => 'Dicky Puja',
        ]);

        DB::table('supervisors')->insert([
            'name' => 'Bayu Hendra W',
        ]);

        DB::table('supervisors')->insert([
            'name' => 'Anandia M Yudhistira',
        ]);
    }
}
