<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JobStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('job_statuses')->insert([
            'name' => 'Employed',
        ]);

        DB::table('job_statuses')->insert([
            'name' => 'Contract',
        ]);

        DB::table('job_statuses')->insert([
            'name' => 'Freelance',
        ]);

        DB::table('job_statuses')->insert([
            'name' => 'Intern',
        ]);
    }
}
