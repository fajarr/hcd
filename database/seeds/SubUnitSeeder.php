<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubUnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('sub_units')->insert([
            'name' => 'Yogyakarta',
        ]);

        DB::table('sub_units')->insert([
            'name' => 'Jakarta',
        ]);

        DB::table('sub_units')->insert([
            'name' => 'Bandung',
        ]);
    }
}
