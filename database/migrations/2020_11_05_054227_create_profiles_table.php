<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->id();
            $table->string('telegram')->nullable();
            $table->string('bpjs_kes')->nullable();
            $table->string('bpjs_ket')->nullable();
            $table->enum('gol_darah', ['a', 'b', 'ab', 'o'])->nullable();
            $table->string('ijazah')->nullable();
            $table->string('nokk')->nullable();
            $table->string('npwp')->nullable();
            $table->string('payroll')->nullable();
            $table->string('place_of_birth')->nullable();
            $table->string('image')->default('uploads/default.jpg');

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
