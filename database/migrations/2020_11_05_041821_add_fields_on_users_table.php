<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsOnUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('employee_id')->nullable();
            $table->string('other_id')->nullable();
            $table->string('license_number')->nullable();
            $table->date('license_exp')->nullable();
            $table->enum('gender', ['male', 'female'])->nullable();
            $table->enum('marital_status', ['single', 'married'])->nullable();
            $table->enum('nationality', ['indonesian', 'foreign'])->nullable();
            $table->date('dob')->nullable();

            $table->unsignedBigInteger('job_status')->nullable();
            $table->unsignedBigInteger('supervisor_id')->nullable();
            $table->unsignedBigInteger('sub_unit')->nullable();
            $table->foreign('job_status')->references('id')->on('job_statuses');
            $table->foreign('supervisor_id')->references('id')->on('supervisors');
            $table->foreign('sub_unit')->references('id')->on('sub_units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
