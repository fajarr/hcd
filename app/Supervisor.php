<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supervisor extends Model
{
    //
    public function user()
    {
        return $this->hasMany(User::class, 'supervisor_id');
    }
}
