<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Laravolt\Support\Traits\AutoFilter;
use Laravolt\Support\Traits\AutoSort;

class User extends \Laravolt\Platform\Models\User
{
    use Notifiable;
    use AutoSort;
    use AutoFilter;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $table = 'users';

    protected $fillable = [
        'name',
        'middle_name',
        'last_name',
        'employee_id',
        'other_id',
        'license_number',
        'license_exp',
        'email',
        'password',
        'gender',
        'marital_status',
        'nationality',
        'dob',
        'job_status',
        'supervisor_id',
        'sub_unit',
        'status',
    ];

    public function subUnit()
    {
        return $this->belongsTo(SubUnit::class, 'sub_unit');
    }

    public function jobStatus()
    {
        return $this->belongsTo(JobStatus::class, 'job_status');
    }

    public function supervisor()
    {
        return $this->belongsTo(Supervisor::class, 'supervisor_id');
    }

    public function profile()
    {
        return $this->hasOne(Profile::class, 'user_id');
    }

    public function leave()
    {
        return $this->hasMany(Leave::class);
    }

    public function project() {
        return $this->belongsToMany(Project::class, 'projects_users', 'user_id', 'project_id');
    }

    public function article()
    {
        return $this->hasMany(Article::class, 'created_by');
    }
}
