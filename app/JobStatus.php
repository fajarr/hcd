<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobStatus extends Model
{
    //
    public function user()
    {
        return $this->hasMany(User::class, 'job_status');
    }
}
