<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    //
    protected $table = 'projects';

    protected $fillable = [
        'name',
        'description',
        'status',
        'value',
        'start',
        'end',
    ];

    public function user() {
        return $this->belongsToMany(User::class, 'projects_users', 'project_id', 'user_id');
    }
}
