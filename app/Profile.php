<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //
    protected $table = 'profiles';

    protected $fillable = [
        'user_id',
        'telegram',
        'bpjs_kes',
        'bpjs_ket',
        'gol_darah',
        'ijazah',
        'nokk',
        'npwp',
        'payroll',
        'place_of_birth',
        'image',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
