<?php

namespace App\Tables;

use Laravolt\Suitable\Columns\Label;
use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\RestfulButton;
use Laravolt\Suitable\Columns\Text;
use Laravolt\Suitable\TableView;

class ArticleTableView extends TableView
{
    protected $title = 'List of Articles';

    protected function source()
    {
        // return Model::paginate();
    }

    protected function columns()
    {
        return [
            // See https://laravolt.dev/docs/suitable/
            Numbering::make('No'),
            Text::make('title', 'Title'),
            Text::make('category.name', 'Category'),
            Label::make('status', 'Status')
                ->addClassIf('Approve', 'green')
                ->addClassIf('Cancel', 'red'),
            RestfulButton::make('article', 'Action')->except("delete"),
        ];
    }
}
