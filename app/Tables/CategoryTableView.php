<?php

namespace App\Tables;

use App\Category;
use Laravolt\Suitable\Columns\Date;
use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\RestfulButton;
use Laravolt\Suitable\Columns\Text;
use Laravolt\Suitable\TableView;

class CategoryTableView extends TableView
{
    public function source()
    {

    }

    protected function columns()
    {
        return [
            Numbering::make('No'),
            Text::make('name')->sortable(),
            Date::make('created_at', 'Created At'),
            RestfulButton::make('category', 'Action')->only("edit", "delete"),
        ];
    }
}
