<?php

namespace App\Tables;

use Laravolt\Suitable\Columns\Date;
use Laravolt\Suitable\Columns\Label;
use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\Raw;
use Laravolt\Suitable\TableView;
use Laravolt\Suitable\Columns\Text;

class UserProjectTable extends TableView
{
    protected $title = 'Your Projects';

    protected function columns()
    {
        return [
            // See https://laravolt.dev/docs/suitable/
            Numbering::make('No'),
            Text::make('name', 'Project Name')->sortable(),
            Raw::make(function ($project) {
                return "Rp. " . number_format($project->value);
            }, 'Value Of Project')->sortable(),
            Date::make('start', 'Starting Date')->sortable(),
            Date::make('end', 'End Date')->sortable(),
            Label::make('status', 'Status of Project')
                ->addClassIf('in progress', 'yellow')
                ->addClassIf('prospect', 'red')
                ->addClassIf('finished', 'green')->sortable(),
        ];
    }
}
