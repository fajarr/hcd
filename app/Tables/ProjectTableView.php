<?php

namespace App\Tables;

use Laravolt\Suitable\Columns\Raw;
use Laravolt\Suitable\TableView;
use Laravolt\Suitable\Columns\Text;
use Laravolt\Suitable\Columns\RestfulButton;
use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\Date;
use Laravolt\Suitable\Columns\Label;

class ProjectTableView extends TableView
{
    protected $title = "List of Projects";

    protected function columns()
    {
        // TODO: Implement columns() method.
        return [
            Numbering::make('No'),
            Text::make('name', 'Project Name')->sortable(),
            Raw::make(function ($project) {
                return "Rp. " . number_format($project->value);
            }, 'Value Of Project')->sortable(),
            Date::make('start', 'Starting Date')->sortable(),
            Date::make('end', 'End Date')->sortable(),
            Raw::make(function ($project) {
                return $project->user->implode('name', ' , ');
            }, 'Managed By'),
            Label::make('status', 'Status of Project')
                ->addClassIf('in progress', 'yellow')
                ->addClassIf('prospect', 'red')
                ->addClassIf('finished', 'green')->sortable(),
            RestfulButton::make('project', 'Action'),
        ];
    }
}
