<?php

namespace App\Tables;

use Laravolt\Suitable\TableView;
use Laravolt\Suitable\Columns\Text;
use Laravolt\Suitable\Columns\RestfulButton;
use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\Date;
use Laravolt\Suitable\Columns\Label;

class LeaveTableView extends TableView
{
    protected $title = "List of Leave Submissions";

    protected function columns()
    {
        // TODO: Implement columns() method.
        return [
            Numbering::make('No'),
            Date::make('start', 'Starting Date')->sortable(),
            Text::make('category', 'Category Name')->sortable(),
            Text::make('information', 'Information of Leaving'),
            Label::make('status', 'Status of Submission')
                ->addClassIf('pending', 'yellow')
                ->addClassIf('accepted', 'green')
                ->addClassIf('rejected', 'red')->sortable(),
            RestfulButton::make('leave', 'Action'),
        ];
    }
}
