<?php

namespace App\Tables;

use Laravolt\Suitable\Columns\Avatar;
use Laravolt\Suitable\Columns\Raw;
use Laravolt\Suitable\TableView;
use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\Text;
use Laravolt\Suitable\Columns\RestfulButton;

class UserTableView extends TableView
{
    protected $title = "List of Employees";

    protected function columns()
    {
        // TODO: Implement columns() method.
        return [
            Numbering::make('No'),
            Avatar::make('name', ''),
            Raw::make(function ($user) {
                $name = $user->name . ' ' . $user->middle_name;

                return $name;
            }, 'First & Middle Name')->sortable(),
            Text::make('last_name', 'Last Name')->sortable(),
            Text::make('employee_id', 'Employee ID')->sortable(),
            Text::make('jobStatus.name', 'Employment Status')->sortable(),
            Text::make('subUnit.name', 'Sub Unit')->sortable(),
            Text::make('supervisor.name', 'Supervisor')->sortable(),
            RestfulButton::make('user', 'Action')
        ];
    }
}
