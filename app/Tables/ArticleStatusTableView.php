<?php

declare(strict_types=1);

namespace App\Tables;

use Laravolt\Suitable\Columns\Date;
use Laravolt\Suitable\Columns\Label;
use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\Raw;
use Laravolt\Suitable\Columns\RestfulButton;
use Laravolt\Suitable\Columns\Text;
use Laravolt\Suitable\TableView;

class ArticleStatusTableView extends TableView
{
    protected $title = 'List Of Articles';

    protected function source()
    {
        // return Model::paginate();
    }

    protected function columns()
    {
        return [
            // See https://laravolt.dev/docs/suitable/
            Numbering::make('No')->sortable(),
            Raw::make(function ($article) {
                $name = $article->user->name . " " . $article->user->middle_name . " " . $article->user->last_name;
                return $name;
            }, 'Writer')->sortable(),
            Text::make('title', 'Title')->sortable(),
            Text::make('category.name', 'Category')->sortable(),
            Date::make('created_at', 'Created At')->sortable(),
            Label::make('status', 'Status')
                ->addClassIf('Approve', 'green')
                ->addClassIf('Cancel', 'red')->sortable(),
            RestfulButton::make('article-status', 'Action')->except("edit"),
        ];
    }
}
