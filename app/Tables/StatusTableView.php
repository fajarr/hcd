<?php

namespace App\Tables;

use Laravolt\Suitable\Columns\Raw;
use Laravolt\Suitable\TableView;
use Laravolt\Suitable\Columns\Text;
use Laravolt\Suitable\Columns\RestfulButton;
use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\Date;
use Laravolt\Suitable\Columns\Label;

class StatusTableView extends TableView
{
    protected $title = "List of Leave Submissions";

    protected function columns()
    {
        // TODO: Implement columns() method.
        return [
            Numbering::make('No'),
            Raw::make(function ($leave) {
                $name = $leave->user->name . ' ' . $leave->user->middle_name . ' ' . $leave->user->last_name;

                return $name;
            }, 'Submission From')->sortable(),
            Text::make('user.employee_id', 'Employee ID')->sortable(),
            Date::make('start', 'Starting Date')->sortable(),
            Text::make('category', 'Leave Category')->sortable(),
            Text::make('information', 'Information of Leaving'),
            Label::make('status', 'Status of Submission')
                ->addClassIf('pending', 'yellow')
                ->addClassIf('accepted', 'green')
                ->addClassIf('rejected', 'red')->sortable(),
            RestfulButton::make('status', 'Action')->only("edit", "delete"),
        ];
    }
}
