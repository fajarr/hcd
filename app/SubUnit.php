<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubUnit extends Model
{
    //
    public function user()
    {
        return $this->hasMany(User::class, 'user_id');
    }
}
