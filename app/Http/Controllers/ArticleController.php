<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\Tables\ArticleTableView;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return ArticleTableView
     */
    public function index()
    {
        //
        $getArticleOfUser = Auth::user()->article;
        return (ArticleTableView::make($getArticleOfUser))->view('article.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|Response|View
     */
    public function create()
    {
        //
        $category = Category::pluck('name', 'id');
        return view('article.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        $article = $request->all();
        $article['created_by'] = Auth::user()->id;
        $article['updated_by'] = Auth::user()->id;

        $article['image'] = $article['image']->store('uploads', 'public');


        Article::create($article);

        return redirect()->back()->withSuccess('A new article has been made successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|Response|View
     */
    public function show($id)
    {
        //
        $article = Article::find($id);
        return view('article.show', ['article' => $article]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|Response|View
     */
    public function edit($id)
    {
        //
        $article = Article::find($id);
        $category = Category::pluck('name', 'id');
        return view('article.edit', ['article' => $article, 'category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        $article = $request->all();
        $article['updated_by'] = Auth::user()->id;

        if ($request->hasFile('image')) {
            Storage::delete(public_path('storage/' . $article['image']));
            $article['image'] = $article['image']->store('uploads', 'public');
        } else {
            $article['image'] = Article::find($id)->image;
        }

        Article::find($id)->update($article);

        return redirect()->back()->withSuccess('This article has been updated successfully!');
    }
}
