<?php

namespace App\Http\Controllers;

use App\Leave;
use App\Tables\StatusTableView;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;

class StatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return StatusTableView
     */
    public function index()
    {
        //
        $leave = Leave::paginate();
        return (StatusTableView::make($leave))->view('leave.status.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Application|Factory|Response|View
     */
    public function show($id)
    {
        //
        $leave = Leave::find($id);
        return view('leave.status.show', ['leave' => $leave]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Application|Factory|Response|View
     */
    public function edit($id)
    {
        //
        $leave = Leave::find($id);
        return view('leave.status.edit', ['leave' => $leave]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $leave = Leave::find($id);
        $leave->delete();

        return redirect()->back()->withSuccess('This submission has been deleted!');
    }

    public function approve($id)
    {
        $leave = Leave::find($id);
        $leave->status = 'accepted';
        $leave->save();

        return redirect()->back()->withSuccess('This submission has been accepted!');
    }

    public function decline($id)
    {
        $leave = Leave::find($id);
        $leave->status = 'rejected';
        $leave->save();

        return redirect()->back()->withSuccess('This submission has been rejected!');
    }
}
