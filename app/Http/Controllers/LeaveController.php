<?php

namespace App\Http\Controllers;

use App\Leave;
use App\Tables\LeaveTableView;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class LeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return LeaveTableView
     */
    public function index()
    {
        //
        $getLeaveOfUser = Auth::user()->leave;
        return (LeaveTableView::make($getLeaveOfUser))->view('leave.index');
    }

    /**
     * Show the form for crelating a new resource.
     *s
     * @return Application|Factory|Response|View
     */
    public function create()
    {
        //
        return view('leave.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        $leave = $request->all();
        $leave['status'] = 'pending';
        $leave['user_id'] = Auth::user()->id;
        Leave::create($leave);

        return redirect()->back()->withSuccess('Leave has been submitted!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
        $leave = Leave::find($id);
        return view('leave.show', ['leave' => $leave]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $leave = Leave::find($id);
        return view('leave.edit', ['leave' => $leave]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        $leave = $request->all();
        Leave::find($id)->update($leave);

        return redirect()->back()->withSuccess('This submission has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $leave = Leave::find($id);
        $leave->delete();

        return redirect()->back()->withSuccess('This submission has been deleted!');
    }
}
