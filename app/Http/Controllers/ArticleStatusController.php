<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\Tables\ArticleStatusTableView;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class ArticleStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return ArticleStatusTableView
     */
    public function index()
    {
        //
        $article = Article::paginate();
        return (ArticleStatusTableView::make($article))->view('article.status.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|Response|View
     */
    public function show($id)
    {
        //
        $article = Article::find($id);
        return view('article.status.show', ['article' => $article]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $article = Article::find($id);
        $article->delete();

        return redirect()->back()->withSuccess('This article has been deleted!');
    }

    public function accept($id)
    {
        $article = Article::find($id);
        $article->status = 'Approve';
        $article->save();

        return redirect()->back()->withSuccess('This article has been approved!');
    }

    public function cancel($id)
    {
        $article = Article::find($id);
        $article->status = 'Cancel';
        $article->save();

        return redirect()->back()->withSuccess('This article has been cancelled!');
    }
}
