<?php

namespace App\Http\Controllers;

use App\Http\Requests\Project\Store;
use App\Project;
use App\Tables\ProjectTableView;
use App\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return ProjectTableView
     */
    public function index()
    {
        //
        $project = Project::paginate();
        return (ProjectTableView::make($project))->view('project.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|Response|View
     */
    public function create()
    {
        //
        $user = User::all()->pluck('name', 'id');
        return view('project.create', ['user' => $user]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return Response
     */
    public function store(Store $request)
    {
        //
        $project = Project::create($request->all());
        $project->user()->attach($request->user);

        return redirect()->back()->withSuccess('A new project has been added!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Application|Factory|Response|View
     */
    public function show($id)
    {
        //
        $project = Project::find($id);
        $user = $project->user->implode('name', ', ');
        return view('project.show', compact('project', 'user'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Application|Factory|Response|View
     */
    public function edit($id)
    {
        //
        $project = Project::find($id);
        $user = User::all()->pluck('name', 'id');
        return view('project.edit', compact('project', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return Response
     */
    public function update(Store $request, $id)
    {
        //
        $input = $request->all();
        $project = Project::find($id);
        $project->update($input);
        $project->user()->sync($request->user);

        return redirect()->back()->withSuccess('This project has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $project = Project::find($id);
        $project->delete();

        return redirect()->back()->withSuccess('A project has been deleted!');
    }
}
