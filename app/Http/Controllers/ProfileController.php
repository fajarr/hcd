<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Laravolt\Epicentrum\Http\Requests\My\Password\Update;


class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     */
    public function index()
    {
        //
        $user = auth()->user();

        return view('profile.index', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Application|Factory|Response|View
     */
    public function edit()
    {
        //
        $user = auth()->user();

        return view('profile.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request)
    {
        //
        $user = auth()->user()->profile;
        $input = $request->all();

        if ($request->hasFile('image')) {
            $ext = $request->file('image')->getClientOriginalExtension();
            $imgSave = strtolower($user->user->name) . '_' . time() . '.' . $ext;
            $input['image'] = $input['image']->storeAs('uploads', $imgSave, 'public');
        } else {
            $input['image'] = $user->image;
        }

        $user->update($input);

        return redirect()->back()->withSuccess('Your profile has been updated!');
    }

    public function editPassword()
    {
        $user = auth()->user();

        return view('profile._password', compact('user'));
    }

    public function updatePassword(Update $request)
    {
        //
        if (app('hash')->check($request->password_current, auth()->user()->password)) {
            auth()->user()->setPassword($request->password);

            return redirect()->back()->withSuccess(trans('laravolt::message.password_updated'));
        } else {
            return redirect()->back()->withError(trans('laravolt::message.current_password_mismatch'));
        }
    }
}
