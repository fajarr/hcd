<?php

namespace App\Http\Controllers;

use App\Tables\UserProjectTable;
use Illuminate\Http\Request;

class UserProjectController extends Controller
{
    //
    public function project() {
        $user = auth()->user()->project()->get();
        return (UserProjectTable::make($user))->view('project.user.show');
    }
}
