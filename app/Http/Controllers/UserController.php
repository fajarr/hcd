<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Laravolt\Epicentrum\Repositories\RepositoryInterface;

class UserController extends Controller
{
    protected $repository;

    /**
     * UserController constructor.
     *
     * @param RepositoryInterface $repository
     */
    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        //
        $users = $this->repository->paginate($request);

        return config('laravolt.epicentrum.table_view')::make($users)->view('laravolt::users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|Response|View
     */
    public function create()
    {
        //
        $statuses = $this->repository->availableStatus();
        $roles = app('laravolt.epicentrum.role')->all()->pluck('name', 'id');
        $multipleRole = config('laravolt.epicentrum.role.multiple');

        return view('laravolt::users.create', compact('statuses', 'roles', 'multipleRole'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //save new user to db
        $input = $request->all();
        $input['email'] = strtolower($request->name) . '@javan.com';
        $input['password'] = 'password';
        $roles = $request->get('roles', []);
        $user = $this->repository->createByAdmin($input, $roles);

        //create a profile to a new user
        Profile::create([
            'user_id' => $user->id
        ]);

        return redirect()->route('epicentrum::users.index')->withSuccess(trans('laravolt::message.user_created'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Application|Factory|Response|View
     */
    public function show($id)
    {
        //
        $user = User::with('profile')->find($id);

        return view('vendor.laravolt.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function edit($id)
    {
        //
        $user = $this->repository->findById($id);

        return view('laravolt::account.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->all();
        User::find($id)->update($input);

        return redirect()->back()->withSuccess(trans('laravolt::message.account_updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $user = User::find($id);
        $user->delete();

        return redirect(route('epicentrum::users.index'))->withSuccess(trans('laravolt::message.user_deleted'));
    }
}
